export default class Appartement {
  loyer: number;
  nbPieces: number;
  superficie: number;
  tempsTrajet: number;
  lien: string;
  photo: string;
  meuble: boolean;
  loyerMetreCarre: number;
  loyerNormal: number;
  loyerCaf: number;

  constructor(
    loyer: number,
    nbPieces: number,
    superficie: number,
    tempsTrajet: number,
    lien: string,
    photo: string,
    meuble: boolean
  ) {
    this.loyerNormal = loyer;
    this.loyer = loyer;
    this.loyerCaf = loyer - 221;
    this.loyerMetreCarre = loyer / superficie;

    this.nbPieces = nbPieces;
    this.superficie = superficie;

    this.tempsTrajet = tempsTrajet > 0 ? tempsTrajet : Number.MAX_SAFE_INTEGER;

    this.lien = lien;
    this.photo = photo;
    this.meuble = meuble;
  }

  comparerSuperficie(app: Appartement, inverse: boolean | undefined): number {
    if (this.superficie === app.superficie) {
      return 0;
    }

    const codeRetour: number = this.superficie < app.superficie ? -1 : 1;

    return inverse ? codeRetour * -1 : codeRetour;
  }

  comparerLoyerMetreCarre(
    app: Appartement,
    inverse?: boolean | undefined
  ): number {
    if (this.loyerMetreCarre === app.loyerMetreCarre) {
      return 0;
    }

    const codeRetour: number =
      this.loyerMetreCarre < app.loyerMetreCarre ? -1 : 1;

    return inverse ? codeRetour * -1 : codeRetour;
  }

  comparerLoyer(app: Appartement, inverse: boolean | undefined): number {
    if (this.loyer === app.loyer) {
      return 0;
    }

    const codeRetour: number = this.loyer < app.loyer ? -1 : 1;

    return inverse ? codeRetour * -1 : codeRetour;
  }

  comparerTempsTrajet(app: Appartement, inverse: boolean | undefined): number {
    if (this.tempsTrajet === app.tempsTrajet) {
      return 0;
    }

    const codeRetour: number = this.tempsTrajet < app.tempsTrajet ? -1 : 1;

    return inverse ? codeRetour * -1 : codeRetour;
  }

  getTempsTrajet(): string {
    const valeur: string =
      this.tempsTrajet !== Number.MAX_SAFE_INTEGER
        ? this.tempsTrajet.toString(10)
        : '∞';

    const unite: string = this.tempsTrajet > 1 ? 'mins' : 'min';
    return `${valeur} ${unite}`;
  }

  getLoyer(): string {
    return `${this.loyer} €`;
  }

  getLoyerMetreCarre(): string {
    return `${this.loyerMetreCarre.toFixed(2)} €/m²`;
  }

  getSuperficie(): string {
    return `${this.superficie} m² (${this.getLoyerMetreCarre()})`;
  }

  getNbPieces(): string {
    const unite = this.nbPieces > 1 ? 'pièces' : 'pièce';
    return `${this.nbPieces} ${unite}`;
  }

  deduireCaf() {
    this.loyer = this.loyerCaf;
    this.loyerMetreCarre = this.loyerCaf / this.superficie;
  }

  remettreLoyerNormal() {
    this.loyer = this.loyerNormal;
    this.loyerMetreCarre = this.loyerNormal / this.superficie;
  }
}

export const trierSuperficie = (
  listeAppartements: Appartement[],
  inverse?: boolean
): Appartement[] => {
  return listeAppartements.sort((a, b) => a.comparerSuperficie(b, inverse));
};

export const trierLoyer = (
  listeAppartements: Appartement[],
  inverse?: boolean
): Appartement[] => {
  return listeAppartements.sort((a, b) => a.comparerLoyer(b, inverse));
};

export const trierLoyerMetreCarre = (
  listeAppartements: Appartement[],
  inverse?: boolean
): Appartement[] => {
  return listeAppartements.sort((a, b) =>
    a.comparerLoyerMetreCarre(b, inverse)
  );
};

export const trierTempsTrajet = (
  listeAppartements: Appartement[],
  inverse?: boolean
): Appartement[] => {
  return listeAppartements.sort((a, b) => a.comparerTempsTrajet(b, inverse));
};
